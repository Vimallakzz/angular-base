import { Injectable } from '@angular/core';
import { isArray, isObject, deepCopy } from '../../../core/utils/functions';
import { FormBuilder, FormControl, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CustomFormBuilderService {

  constructor(private formBuilder: FormBuilder) { }

  convertToFormGroup(formObj, initialObject = {}) {
    formObj = deepCopy(formObj);
    const convertedObj = {};
    let groupValidations: any;
    Object.keys(formObj).map((key: string) => {
      if (key === 'validations') {
        groupValidations = formObj[key];
      } else {
        if (isObject(formObj[key])) {
          convertedObj[key] = this.convertToFormGroup(formObj[key], initialObject[key]);
        } else if (isArray(formObj[key])) {
          const initialValue = formObj[key][0];
          if (isObject(initialValue)) {
            const formArrayLength = formObj[key].length;
            const length = initialObject[key] ? initialObject[key].length : 0;
            if (formArrayLength > length) {
              const array = [];
              for (let index = 0; index < formArrayLength; index++) {
                array.push(this.convertToFormGroup(formObj[key][index]));
              }
              convertedObj[key] = this.formBuilder.array(array);
            } else {
              const array = [];
              for (let index = 0; index < length; index++) {
                array.push(this.convertToFormGroup(initialValue, initialObject[key][index]));
              }
              convertedObj[key] = this.formBuilder.array(array);
            }
          } else {
            if (initialValue instanceof FormControl) {
              const value = initialValue.value;
              const validations = formObj[key][1];
              convertedObj[key] = [value, validations];
            } else if (initialValue instanceof FormArray) {
              const value = initialValue;
              convertedObj[key] = value;
            } else {
              convertedObj[key] = formObj[key];
            }
          }
        } else {
          convertedObj[key] = formObj[key];
        }
      }
    });
    return this.formBuilder.group(convertedObj, {validators: groupValidations});
  }
}
