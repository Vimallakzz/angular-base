export const isEmpty = value => value === undefined || value === null || value === '' || value.length === 0;

export function trimTrailingSpace(value) {
  value = value && value.replace(/^\s+/ig, '');
  return value ? value.replace(/\s{2,}/ig, ' ') : '';
}

export function required({value}): {message: string} | null {
  if (value && typeof value === 'string') {
    value = trimTrailingSpace(value);
  }
  if (isEmpty(value)) {
    return {message: 'Required'};
  }
}

export const minLength = (min) => ({value}): {message: string} | null => {
  if (!isEmpty(value) && value.length < min) {
    return {message: `Must be atleast ${min} characters`};
  }
};

export const maxLength = (max) => ({value}): {message: string} | null => {
  if (!isEmpty(value) && value.length > max) {
    return {message: `Must be more than ${max} characters`};
    // return `${i18n.t('validationMessage.MUST_BE_NO_MORE_THAN')} ${max} ${i18n.t('validationMessage.CHARACTERS')}`;
  }
};

export const isInteger = ({value}): {message: string} | null => {
  if (!isEmpty(value) && !Number.isInteger(Number(value))) {
    return {message: `Must be an integer`};
  }
};

export const minValue = (min) => ({value}): {message: string} | null => {
  if (!isEmpty(value) && Number.isInteger(Number(value)) && Number(value) < min) {
    return {message: `Value must be greater than ${min}`};
  }
};

export const maxValue = (max) => ({value}): {message: string} | null => {
  if (!isEmpty(value) && Number.isInteger(Number(value)) && Number(value) > max) {
    return {message: `Value must be less than ${max}`};
  }
};
